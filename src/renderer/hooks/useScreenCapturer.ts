import { desktopCapturer } from "electron"
import { useEffect, useState } from "react"

const CHUNK_DURATION = 100 // ms

const useScreenCapturer = () => {
  const [mediaRecorder, setMediaRecorder] = useState(null)

  const [recordedChunks, setRecordedChunks] = useState([])
  const addChunk = chunk => setRecordedChunks(chunks => [...chunks, chunk])
  const resetChunks = () => setRecordedChunks([])

  useEffect(() => {

    const setUpMediaRecorder = async () => {
      const sources = await desktopCapturer.getSources({ types: ['window', 'screen'] })
      const screen = sources.find(source => source.name === "Screen 1")

      const mediaStream = await navigator.mediaDevices.getUserMedia({
        video: {
          mandatory: { 
            chromeMediaSource: 'desktop',
            chromeMediaSourceId: screen.id,
          },
        },
      })

      const newMediaRecorder = new MediaRecorder(
        mediaStream, 
        { mimeType: "video/webm; codecs=vp9" },
      )

      newMediaRecorder.ondataavailable = ({ data }) => addChunk(data)
      // mediaRecorder.onstop = async e => sendChunks(recordedChunks)
      return newMediaRecorder
    }

    setUpMediaRecorder().then(setMediaRecorder)
  }, [])

  const startRecording = () => {
    if (!mediaRecorder) {
      console.log("Could not start recording - MediaRecorder still setting up")
      return
    }
    console.log("mediaRecorder: ", mediaRecorder);
    
    mediaRecorder.start(CHUNK_DURATION)
  }

  useEffect(() => {
    console.log("screenCapturer recorded chunks: ", recordedChunks);
  }, [recordedChunks])

  const stopRecording = () => mediaRecorder.stop()

  // TODO: Does this
  return {
    isRecording: mediaRecorder && mediaRecorder.state === "recording"
    recordedChunks,
    start: startRecording,
    stop: stopRecording,
  }
}
export default useScreenCapturer