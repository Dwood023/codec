import { useEffect, useState } from "react"

const useWebSocket = (url) => {
  const [socket, setSocket] = useState(null)

  useEffect(() => {
    setSocket(new WebSocket(url))

    return () => socket.close()
  }, [])

  return socket
}
export default useWebSocket