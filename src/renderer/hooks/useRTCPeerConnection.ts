import { useEffect, useState } from "react"
import { useWebSocket } from "/hooks"

interface RTCPeerConnectionOptions {
  onMessage: () => any;
}

const TEST_FREQUENCY = 124.2

const useRTCPeerConnection = (
  {
    onMessage = () => null,
  }: RTCPeerConnectionOptions
) => {
  const [rtcPeerConnection, setRtcPeerConnection] = useState(null)
  const [dataChannel, setDataChannel] = useState(null)

  const setUpDataChannel = dataChannel => {

    dataChannel.onmessage = onMessage

    dataChannel.onopen = e => console.log("DataChannel opened!")

    return dataChannel
  }

  useEffect(() => {
    const newRtcPeerConnection = new RTCPeerConnection()

    newRtcPeerConnection.ondatachannel = ({ channel }) => setDataChannel(setUpDataChannel(channel))

    newRtcPeerConnection.onnegotiationneeded = e => {
      console.log("Negotiation needed...")
    }
    newRtcPeerConnection.onsignalingstatechange = e => {
      console.log("New state: ", newRtcPeerConnection.signalingState)
    }

    setRtcPeerConnection(newRtcPeerConnection) // TODO: Setup STUN/TURN

    return () => newRtcPeerConnection.close()
  }, [])

  const socket = useWebSocket("ws://localhost:8080/ws")

  const send = message => socket.send(JSON.stringify(message))

  const broadcastOffer = (offer, frequency) => send({
    type: "offer",
    payload: {
      frequency: TEST_FREQUENCY,
      offer,
    },
  })

  const broadcastICECandidate = (candidate, frequency) => send({
    type: "ice_candidate",
    payload: {
      frequency,
      candidate,
    },
  })

  const broadcastAnswer = (answer, frequency) => send({
    type: "answer", 
    payload: { 
      answer, 
      frequency, 
    }, 
  })

  const listenOnFrequency = (frequency) => send({
    type: "listen",
    payload: {
      frequencies: [ frequency ],
    }
  })

  useEffect(() => {

    if (!socket) {
      console.log("Socket not open yet, will try again shortly...")
      return
    }

    socket.onopen = e => {

      // Register as listener for offers on frequency
      listenOnFrequency(TEST_FREQUENCY)

      console.log("Registered on frequency: ", TEST_FREQUENCY)
    }

    socket.onmessage = e => {
      const message = JSON.parse(e.data)

      switch (message.type) {
      case "offer": {
        const {
          frequency,
          offer,
        } = message.payload

        console.log("Offer received!");

        rtcPeerConnection.setRemoteDescription(offer)
        console.log("Set RemoteDescription: ", rtcPeerConnection.remoteDescription)

        rtcPeerConnection.createAnswer().then(answer => {

          rtcPeerConnection.setLocalDescription(answer)
          console.log("Set LocalDescription: ", rtcPeerConnection.localDescription)
          console.log("Sending answer!");

          broadcastAnswer(answer, frequency)
        })
        break
      }
      case "answer": {
        const {
          answer,
        } = message.payload

        console.log("Answer received: ", answer)

        rtcPeerConnection.setRemoteDescription(answer)
        console.log("Set RemoteDescription: ", rtcPeerConnection.remoteDescription)

        break
      }
      case "ice_candidate": {
        const {
          candidate,
        } = message.payload

        rtcPeerConnection.addIceCandidate(candidate)
        console.log("Received ICE candidate: ", candidate)
        break
      }
      }
    }
  }, [socket])

  const connectToPeer = () => {
    if (!socket) {
      console.log("Signalling failed (or incomplete), aborting connection to peer.")
      return
    }

    const newChannel = rtcPeerConnection.createDataChannel("testChannel")
    console.log("Created dataChannel: ", newChannel)
    setDataChannel(setUpDataChannel(newChannel))

    rtcPeerConnection.onicecandidate = ({ candidate }) => {
      if (!candidate) {
        console.log("Final candidate recieved.")
        return 
      }
      console.log("Received candidate, will send to peer: ", candidate)

      broadcastICECandidate(candidate, TEST_FREQUENCY)
    }

    rtcPeerConnection.createOffer().then(offer => {

      console.log("Created offer, will send to peer: ", offer)
      
      rtcPeerConnection.setLocalDescription(offer)
      console.log("Set LocalDescription: ", rtcPeerConnection.localDescription)
      broadcastOffer(offer, TEST_FREQUENCY)
    })
  }

  const sendToPeer = async (chunks: [Blob])  => {
    if (!dataChannel || dataChannel.readyState === "connecting") { 
      return
    }

    console.log("Sending chunks to peer: ", chunks)

    const buffers = await Promise.all(chunks.map(chunk => chunk.arrayBuffer()))
    const messages = [...buffers, "DONE"]

    for (const message of messages) { 
      console.log("Sending: ", message)
      dataChannel.send(message)
    }
  }

  return {
    connectToPeer,
    sendToPeer,
  }
}

export default useRTCPeerConnection