import React, { useEffect, useState } from 'react'
import { ipcRenderer } from "electron"
import styled, { keyframes } from "styled-components"
import { channels } from "../../constants"
import {
  useList,
  useRTCPeerConnection,
  useScreenCapturer,
} from "/hooks"

const Circle = styled.circle`
  fill: red;
`
const flash = keyframes`
  from { opacity: 0; }
  to { opacity: 1; }
`
const Svg = styled.svg`
  height: 1rem;
  width: 1rem;
`
const Chunk = styled.div`
  height: 1rem;
  width: 1rem;
  background-color: grey;
`

const Component = () => {
 
  const [isRecording, setIsRecording] = useState(false)
  const [receivedChunks, setReceivedChunks] = useState([])
  const [objectURL, setObjectURL] = useState(null)
  const [readyToEncode, setReadyToEncode] = useState(false)

  const screenCapturer = useScreenCapturer()
  const rtcPeerConnection = useRTCPeerConnection({
    onMessage: ({ data }) => {
      console.log("Received message from peer: ", data, " of type ", typeof data)
      if (data !== "DONE")
        setReceivedChunks(receivedChunks => [...receivedChunks, data])
      else
        setReadyToEncode(true)
    },
  })

  useEffect(() => {
    if (!readyToEncode) 
      return

    const blob = new Blob([...receivedChunks], { type: "video/webm" })
    const objectURL = URL.createObjectURL(blob)

    console.log("Received all chunks, createdObjectURL: ", objectURL)

    setObjectURL(objectURL)
    setReceivedChunks([])
    setReadyToEncode(false)
  }, [readyToEncode])

  useEffect(() => {
    ipcRenderer.on("START_RECORDING", screenCapturer.start)
    ipcRenderer.on("STOP_RECORDING", screenCapturer.stop)

    return () => {
      ipcRenderer.off("START_RECORDING", screenCapturer.start)
      ipcRenderer.off("STOP_RECORDING", screenCapturer.stop)
    }
  }, [screenCapturer])

  const sendRecordedChunksToPeer = () => rtcPeerConnection.sendToPeer(screenCapturer.recordedChunks)

  return (
    <div>
      <button onClick={rtcPeerConnection.connectToPeer}>
        Connect to peer
      </button>
      {
        screenCapturer && ( !screenCapturer.isRecording 
          ? <button onClick={screenCapturer.start}>Start Recording</button>
          : <button onClick={screenCapturer.stop}>Stop Recording</button>
        )
      }
      <button onClick={sendRecordedChunksToPeer}>
        Send chunks
      </button>
      {
        screenCapturer && screenCapturer.recordedChunks.map((chunk, i) => <Chunk key={i} />)
      }
      {
        objectURL && <video autoPlay src={objectURL} />
      }
    </div>
  )
}

export default Component