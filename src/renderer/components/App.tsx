import React from 'react'
import Component from "/components/Component"

const App = () => (
  <div>
    <Component />
  </div>
)

export default App