import { app, BrowserWindow, ipcMain, nativeImage, Tray, Menu } from "electron"
import chokidar from "chokidar"
import logo from "../media/white.png"
import path from "path"
import { channels } from "../constants"

let mainWindow: BrowserWindow
let tray: Tray

// Give existing instances signal to quit
app.requestSingleInstanceLock()
app.on("second-instance", app.quit)

process.on("SIGINT", () => console.log("ASDASD"))

app.on("ready", async () => {

  tray = new Tray(path.join(__dirname, logo)) // For some reason 

  // const contextMenu = Menu.buildFromTemplate([
  //   { label: 'Item1', type: 'radio' },
  //   { label: 'Item2', type: 'radio' }
  // ])

  // tray.setToolTip('This is my application.')
  // tray.setContextMenu(contextMenu)
  let isRecording = false

  const startRecording = () => {
    mainWindow.webContents.send("START_RECORDING")
    isRecording = true
  }
  const stopRecording = () => {
    mainWindow.webContents.send("STOP_RECORDING")
    isRecording = false
  }
  ipcMain.on('reply', (event, arg) => {
    console.log("Received blob from renderer: ", arg);
  })

  tray.on('click', () => isRecording ? stopRecording() : startRecording())

  mainWindow = new BrowserWindow({
    frame: false,
    transparent: false,
    x: 100,
    y: 850,
    height: 400,
    width: 400,
    webPreferences: {
      nodeIntegration: true
    }
  })

  const url = `file://${__dirname}/../renderer/index.html`
  console.log("Renderer source url: ", url);
  mainWindow.webContents.openDevTools()
  

  const watcher = chokidar.watch("./**").on('change', () => {
    console.log("Reloading renderer source!");
    loadRendererSource()
  })

  const loadRendererSource = () => {
    mainWindow.loadURL(url)
  }
  // mainWindow.on("closed", () => (mainWindow = null))
})

// Record screen


app.on("window-all-closed", app.quit)